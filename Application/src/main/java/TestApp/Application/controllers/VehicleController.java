package TestApp.Application.controllers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
*
* A simple interface for interacting with the Virtual Vehicles API.
* Uses a base url of http://localhost:5000.
* Port number can be changed in application.properties under resources.
*/
@RestController
public class VehicleController {

	/* Filename for the .properties file */
	final private String FILENAME = "application.properties";
	
	/* Temporary admin key */
	private String adminKey;
	
	/* Used on startup to get some initial properties */
	Properties prop = getProperties();
	
	private String user;
	
	private String pass;
	
	/**
     * EX: "http://localhost:5000/viewvehicles?adminKey={adminkey}"
     * @return retString
     */
    @RequestMapping(value = "/viewvehicles", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public String viewVehicle(@RequestParam(value = "adminKey")String adminKey) {
    	if(adminKey.compareTo(this.adminKey) != 0) {
    		return "Error: invalid admin key!";
    	}
    	String output = "";
    	String retString = "Vehicles:<br/>----------------<br/>";
    	JSONArray myArr = null;
    	JSONObject obj = null;
    	
    	try {
    		URL url = new URL("http://localhost:8581/vehicles");
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    				
    		conn.setRequestMethod("GET");
    	    conn.setRequestProperty("Accept", "application/json");
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());
    	    BufferedReader br = new BufferedReader(new InputStreamReader(
    	                    (conn.getInputStream())));

    	    output = br.readLine();
    	           
    	    obj = new JSONObject(output); 
    	    br.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    	}
    	
    	try {
	    	myArr = obj.getJSONObject("_embedded").getJSONArray("vehicles");
	    		
	    	for(int i = 0; i < myArr.length(); i++) {
	    		JSONObject vehicle = myArr.getJSONObject(i);
	    		retString += vehicleToString(vehicle) + "<br/>";
	
	    	}
    	} catch(Exception e) {
    		System.out.println(e);
    		return "Sorry, an internal error has occurred";
    	}
        return retString;
    }
        
    /**
     * EX: "http://localhost:5000/readvehicle?oid={ObjectID}"
     * @param theUser
     * @return retString
     */
    @GetMapping("/readvehicle")
    @ResponseStatus(HttpStatus.OK)
    public String readVehicle(@RequestParam(value = "oid")String oid) {
    	
    	String output = "";
    	JSONObject obj = null;
    	try {
    		URL url = new URL("http://localhost:8581/vehicles/" + oid);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    				
    		conn.setRequestMethod("GET");
    	    conn.setRequestProperty("Accept", "application/json");
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());
    	    BufferedReader br = new BufferedReader(new InputStreamReader(
    	                    (conn.getInputStream())));

    	    output = br.readLine();
    	            
    	    obj = new JSONObject(output);
    	    br.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    	}
    	
    	if(obj == null) {
    		return "Error: Object not found";
    	}
    	
		return vehicleToString(obj);
    }
    
    /**
     * EX: "http://localhost:5000/vehiclemake?make={Make}"
     * @param make
     * @return retString
     */
    @GetMapping("/vehiclemake")
    @ResponseStatus(HttpStatus.OK)
    public String nameVehicle(@RequestParam(value = "make")String make) {
    	
    	make = StringEscapeUtils.escapeHtml4(make);
    	
    	String output = "";
    	JSONObject obj = null;
    	JSONArray myArr = null;
    	String retString = "Vehicles with the make: " + make + ":<br/>-----------------------------------------<br/>";
    	try {
    		URL url = new URL("http://localhost:8581/vehicles?filter=make::" + make);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		
    		conn.setRequestMethod("GET");
    		conn.setRequestProperty("Accept", "application/json");
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());
    		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		
    		output = br.readLine();
    		obj = new JSONObject(output);
    		br.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    	}
    	
    	if(obj == null) {
    		return "Error: Object not found";
    	}
    	myArr = obj.getJSONObject("_embedded").getJSONArray("vehicles");
		
    	for(int i = 0; i < myArr.length(); i++) {
    		JSONObject users = myArr.getJSONObject(i);
    		retString += vehicleToStringPartial(users) + "<br/>";

    	}
    	
    	return retString;
    }
    
    /**
     * EX: "http://localhost:5000/addvehicle?year={year}&make={make}&model={model}
     * 							&color={color}&type={type}mileage={mileage}&owner={owner}"
     * @param username
     * @param password
     * @return output
     */
    @GetMapping("/addvehicle")
    @ResponseStatus(HttpStatus.CREATED)
    public String addVehicle(@RequestParam(value = "year")int year, 
    		@RequestParam(value = "make")String make,
            @RequestParam(value = "model")String model,
            @RequestParam(value = "color")String color,
            @RequestParam(value = "type")String type,
            @RequestParam(value = "mileage")int mileage,
            @RequestParam(value = "owner")String owner){
    	String output = "";
    	JSONObject obj = null;
    	try {
    		URL url = new URL("http://localhost:8581/vehicles");
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    				
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
    	    conn.setRequestProperty("Accept", "application/json");
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());
    	    conn.setDoOutput(true);
    	    
    	    JSONObject newApp = new JSONObject();
    	    newApp.put("year", year);
    	    newApp.put("make", make);
    	    newApp.put("model", model);
            newApp.put("color", color);
            newApp.put("type", type);
            newApp.put("mileage", mileage);
            newApp.put("owner", owner);
    	    
    	    OutputStream os = conn.getOutputStream();
    	    os.write(newApp.toString().getBytes("UTF-8"));
    	    os.close();
    	    
    	    BufferedReader br = new BufferedReader(new InputStreamReader(
    	                    (conn.getInputStream())));

    	    output = br.readLine();
    	            
    	    obj = new JSONObject(output); 
    	    br.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    	}
    	
    	return vehicleToString(obj);
    }
    
    /**
     * EX: "http://localhost:5000/deletevehicle?oid={ObjectID}&adminKey={adminkey}"
     * @param oid
     * @return retString
     */
    @GetMapping("/deletevehicle")
    @ResponseStatus(HttpStatus.OK)
    public String deleteVehicle(@RequestParam(value = "oid")String oid, 
    		@RequestParam(value = "adminKey")String adminKey) {
    	if(adminKey.compareTo(this.adminKey) != 0) {
    		return "Error: invalid admin key!";
    	}
    	try {
    		URL url = new URL("http://localhost:8581/vehicles/" + oid);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		
    	    conn.setRequestProperty("Accept", "application/json");
    	    conn.setDoOutput(true);
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());		
    		conn.setRequestMethod("DELETE");
    	    //conn.connect();
    		if(conn.getResponseCode() != 204) {
    			return "Error occurred in deleting vehicle";
    		}
    		System.out.println(conn.getResponseCode());
    	    
    	} catch(Exception e) {
    		System.out.println(e.toString());
    		return "Error occurred in deleting vehicle";
    	}
    	  	
		return "Vehicle successfully deleted.";
    }

    /**
     * localhost:5000/buy?year={year}&make={make}&model={model}
     * 							&color={color}&type={type}mileage={mileage}
     * 
	 * This function is currently bugged and not working
	 *
     * @param year
     * @param make
     * @param model
     * @param color
     * @param type
     * @param mileage
     * @return
     */
    @GetMapping("/buy")
    @ResponseStatus(HttpStatus.OK)
    public String buyVehicle( 
    		@RequestParam(value = "make")String make,
            @RequestParam(value = "model")String model,
            @RequestParam(value = "color")String color) {
    	make = StringEscapeUtils.escapeHtml4(make);
    	model = StringEscapeUtils.escapeHtml4(model);
    	color = StringEscapeUtils.escapeHtml4(color);
    	
    	String output = "";
    	JSONObject obj = null;
    	JSONArray myArr = null;
    	
    	if(user == null || pass == null) {
    		return "Error: not logged in";
    	}
    	
    	try {
    		
    		URL url1 = new URL("http://localhost:8581/vehicles?filter=" 
    																	+ "make::" + make
    																	+ "&model::" + model
    																	+ "&color::" + color);
    		HttpURLConnection conn1 = (HttpURLConnection) url1.openConnection();
    		
    		conn1.setRequestMethod("GET");
    		conn1.setRequestProperty("Accept", "application/json");
    	    conn1.setRequestProperty("Authorization", "Bearer " + getJwt());
    		
    	    BufferedReader br1 = new BufferedReader(new InputStreamReader(conn1.getInputStream()));
    	    
    		output = br1.readLine();
    		obj = new JSONObject(output);
    		br1.close();
    		
    		myArr = obj.getJSONObject("_embedded").getJSONArray("vehicles");
        	if(myArr.length() < 1) {
    			return "Internal server error has occurred";
    		}
    		JSONObject car = myArr.getJSONObject(0);
    		
    		if(car.has("owner")) {
    			return "Sorry, this vehicle is currently owned";
    		}
    		
    		String id = car.getString("id");
        	int year = car.getInt("year");
        	make = car.getString("make");
        	model = car.getString("model");
        	color = car.getString("color");
        	String type = car.getString("type");
        	int mileage = car.getInt("mileage");
        	String createdAt = car.getString("createdAt");
    		
    		URL url2 = new URL("http://localhost:8581/vehicles/" + id);
    		HttpURLConnection conn2 = (HttpURLConnection) url2.openConnection();
    				
    		conn2.setRequestMethod("PUT");
    		conn2.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
    	    conn2.setRequestProperty("Accept", "application/json");
    	    conn2.setRequestProperty("Authorization", "Bearer " + getJwt());
    	    conn2.setDoOutput(true);
    	    
    	    JSONObject newApp = new JSONObject();
    	    newApp.put("year", year);
    	    newApp.put("make", make);
    	    newApp.put("model", model);
            newApp.put("color", color);
            newApp.put("type", type);
            newApp.put("mileage", mileage);
            newApp.put("owner", user);
            newApp.put("createdAt", createdAt);
    	    
    	    OutputStream os = conn2.getOutputStream();
    	    os.write(newApp.toString().getBytes("UTF-8"));
    	    os.close();
    	    
    	    BufferedReader br = new BufferedReader(new InputStreamReader(
    	                    (conn2.getInputStream())));

    	    output = br.readLine();
    	            
    	    obj = new JSONObject(output); 
    	    br.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    		return "Internal error occurred";
    	}
    	
    	return vehicleToStringPartial(obj);
    }

   /**
     * EX: "http://localhost:5000/pingvehicle"
     * @return retString
     */
    @GetMapping("/pingvehicle")
    @ResponseStatus(HttpStatus.OK)
    public String pingVehicle() {
    	String retString = "";
    	try {
    		URL url = new URL("http://localhost:8581/vehicles/utils/ping");
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		
    	   conn.setRequestProperty("Accept", "application/json");	
    		conn.setRequestMethod("GET");
    		if(conn.getResponseCode() == 200) {
    			retString = "Vehicle service is currently running ";
    		}
    	} catch(Exception e) {
    		System.out.println(e.toString());
    		retString = "Warning: Cannot connect to Vehicle Service ";
    	}
    	return retString;
    }

    /**
     * localhost:5000/logon?username={username}&password={password}
     * 
     * @param username
     * @param password
     * @return retString
     */
    @GetMapping("/logon")
    @ResponseStatus(HttpStatus.OK)
    public String logon(String username, String password) {
    	username = StringEscapeUtils.escapeHtml4(username);
    	password = StringEscapeUtils.escapeHtml4(password);
    	
    	String output = "";
    	JSONObject obj = null;
    	JSONArray myArr = null;
    	String retString = username + " has logged on.";
    	try {
    		URL url = new URL("http://localhost:8591/users?filter=username::" + username + "|password::" + password);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		
    		conn.setRequestMethod("GET");
    		conn.setRequestProperty("Accept", "application/json");
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());
    		if(conn.getResponseCode() != 200) {
    			return "Sorry, wrong username/password";
    		}
    		
    		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		
    		output = br.readLine();
    		obj = new JSONObject(output);
    		br.close();
    		
    	} catch(Exception e) {
    		System.out.println(e.toString());
    	}
    	
    	if(obj == null) {
			return "Sorry, wrong username/password";
		}
    	
    	myArr = obj.getJSONObject("_embedded").getJSONArray("users");
    	if(myArr.length() < 1) {
			return "Sorry, wrong username/password";
		}
    	
    	user = username;
    	pass = password;
    	
    	return retString;
    }
    
    /**
     * 
     * EX: "http://localhost:5000/setauth?jwt=sampletoken
     * @param JWT
     * @return retString
     */
    @GetMapping("/setauth")
    @ResponseStatus(HttpStatus.OK)
    public String setAuthentication(@RequestParam(value = "jwt")String JWT) {
    	String retString = "JWT Set";
    	String jwtClean = StringEscapeUtils.escapeHtml4(JWT);
    	try {
    		Properties prop = new Properties();
 		   	InputStream inStream = getClass().getClassLoader().getResourceAsStream(FILENAME);
 		   	FileOutputStream outStream = new FileOutputStream("src/main/resources/application.properties");
 		   	if(inStream != null) {
 			   prop.load(inStream);
 		   	} else {
 			   outStream.close();
 			   throw new FileNotFoundException("property file not found");
 		   	}
    		prop.put("jwt", jwtClean);
    		prop.store(outStream, null);
    		inStream.close();
			outStream.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    		return "Internal error has occurred";
    	}
    	return retString;
    }
    
    /**
     * 
     * EX: "http://localhost:5000/refresh
     * 
     * @return retString
     */
    @GetMapping("/refresh")
    @ResponseStatus(HttpStatus.OK)
    public String refresh() {
    	String retString = "JWT Set";
    	
    	try {
    		Properties prop = new Properties();
 		   	InputStream inStream = getClass().getClassLoader().getResourceAsStream(FILENAME);
 		   	FileOutputStream outStream = new FileOutputStream("src/main/resources/application.properties");
 		   	if(inStream != null) {
 			   prop.load(inStream);
 		   	} else {
 			   outStream.close();
 			   throw new FileNotFoundException("property file not found");
 		   	}
 		   	
 		   	String apikey = prop.getProperty("apikey");
 		   	String secret = prop.getProperty("secret");
 		   	
 		   	URL url = new URL("http://localhost:8587/jwts?apiKey=" + apikey
					 + "&secret=" + secret);
 		   	HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				
 		   	conn.setRequestMethod("GET");
 		   	conn.setRequestProperty("Accept", "application/json");
	    
 		   	BufferedReader br = new BufferedReader(new InputStreamReader(
	                    (conn.getInputStream())));

 		   	String output = br.readLine();
 		   	String jwt = output.substring(1, output.length()-1);
	            
 		   	br.close();
 		   	
    		prop.put("jwt", jwt);
    		prop.store(outStream, null);
    		inStream.close();
			outStream.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    		return "Internal error has occurred";
    	}
    	return retString;
    }

   /**
     * Converts JSON client object to a string detailing each field
     * @param theUser
     * @return retString
     */
   private String vehicleToString(JSONObject theVehicle) {
    	String retString = "";
    	retString += "Object ID: " + theVehicle.getString("id");
    	retString += "<br/>Year: " + theVehicle.getInt("year");
    	retString += "<br/>Make: " + theVehicle.getString("make");
    	retString += "<br/>Model: " + theVehicle.getString("model");
    	retString += "<br/>Color: " + theVehicle.getString("color");
    	retString += "<br/>Type: " + theVehicle.getString("type");
    	retString += "<br/>Mileage: " + theVehicle.getInt("mileage");
    	if(theVehicle.has("owner")) {
    		retString += "<br/>Owner: " + theVehicle.getString("owner");
    	}
    	retString += "<br/>Created At: " + theVehicle.getString("createdAt");
    	retString += "<br/>Updated At: " + theVehicle.getString("updatedAt") + "<br/>";
    	return retString;
    }


   /**
     * Converts JSON client object to a string detailing the year, make,
     * model, color, type, mileage, createdAt, updatedAt.
     
     * @param theClient
     * @return retString
     */
   private String vehicleToStringPartial(JSONObject theVehicle) {
    	String retString = "";
    	retString += "Year: " + theVehicle.getInt("year");
    	retString += "<br/>Make: " + theVehicle.getString("make");
        retString += "<br/>Model: " + theVehicle.getString("model");
        retString += "<br/>Color: " + theVehicle.getString("color");
        retString += "<br/>Type: " + theVehicle.getString("type");
        retString += "<br/>Mileage: " + theVehicle.getInt("mileage");
        retString += "<br/>Owner: " + theVehicle.getString("owner");
    	retString += "<br/>Created At: " + theVehicle.getString("createdAt");
    	retString += "<br/>Updated At: " + theVehicle.getString("updatedAt") + "<br/>";
    	return retString;
    }
   
   /**
    * Loads application.properties to get admin key value
    * 
    * @return prop = properties config
    */
   private Properties getProperties() {
	   InputStream inStream;
	   
	   try {
		   Properties prop = new Properties();
		   inStream = getClass().getClassLoader().getResourceAsStream(FILENAME);
		   
		   if(inStream != null) {
			   prop.load(inStream);
		   } else {
			   throw new FileNotFoundException("property file" + FILENAME + " not found");
		   }
		   
		   this.adminKey = prop.getProperty("adminKey");
		   
		   inStream.close();
	   } catch(Exception e) {
		   System.out.println("Exception: " + e);
	   }
	   
	   return prop;
   }
   
   /**
    * Gets the JWT set in application.properties
    * 
    * @return jwt
    */
   private String getJwt() {
	   String jwt = "";
	   InputStream inStream;
	   
	   try {
		   Properties prop = new Properties();
		   inStream = getClass().getClassLoader().getResourceAsStream(FILENAME);
		   
		   if(inStream != null) {
			   prop.load(inStream);
		   } else {
			   throw new FileNotFoundException("property file" + FILENAME + " not found");
		   }
		   jwt = prop.getProperty("jwt");
		   
		   inStream.close();
	   } catch(Exception e) {
		   System.out.println("Exception: " + e);
	   }
	   return jwt;
   }
}