package TestApp.Application.controllers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
*
* A simple interface for interacting with the Virtual Vehicles API.
* Uses a base url of http://localhost:5000.
* Port number can be changed in application.properties under resources.
*/
@RestController
public class UserController {

	/* Filename for the .properties file */
	final private String FILENAME = "application.properties";
	
	/* Temporary admin key */
	private String adminKey;
	
	/* Used on startup to get some initial properties */
	Properties prop = getProperties();
	
	/**
     * EX: "http://localhost:5000/viewusers?adminKey={adminkey}"
     * @return retString
     */
    @RequestMapping(value = "/viewusers", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public String viewUsers(@RequestParam(value = "adminKey")String adminKey) {
    	if(adminKey.compareTo(this.adminKey) != 0) {
    		return "Error: invalid admin key!";
    	}
    	String output = "";
    	String retString = "Users:<br/>----------------<br/>";
    	JSONArray myArr = null;
    	JSONObject obj = null;
    	
    	try {
    		URL url = new URL("http://localhost:8591/users");
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    				
    		conn.setRequestMethod("GET");
    	    conn.setRequestProperty("Accept", "application/json");
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());
    	    BufferedReader br = new BufferedReader(new InputStreamReader(
    	                    (conn.getInputStream())));

    	    output = br.readLine();
    	           
    	    obj = new JSONObject(output); 
    	    br.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    	}
    	
    	try {
	    	myArr = obj.getJSONObject("_embedded").getJSONArray("users");
	    		
	    	for(int i = 0; i < myArr.length(); i++) {
	    		JSONObject user = myArr.getJSONObject(i);
	    		retString += userToString(user) + "<br/>";
	
	    	}
    	} catch(Exception e) {
    		System.out.println(e);
    		return "Sorry, an internal error has occurred";
    	}
        return retString;
    }
        
    /**
     * EX: "http://localhost:5000/readuser?oid={ObjectID}"
     * @param theUser
     * @return retString
     */
    @GetMapping("/readuser")
    @ResponseStatus(HttpStatus.OK)
    public String readUser(@RequestParam(value = "oid")String oid) {
    	
    	String output = "";
    	JSONObject obj = null;
    	try {
    		URL url = new URL("http://localhost:8591/users/" + oid);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    				
    		conn.setRequestMethod("GET");
    	    conn.setRequestProperty("Accept", "application/json");
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());
    	    BufferedReader br = new BufferedReader(new InputStreamReader(
    	                    (conn.getInputStream())));

    	    output = br.readLine();
    	            
    	    obj = new JSONObject(output);
    	    br.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    	}
    	
    	if(obj == null) {
    		return "Error: Object not found";
    	}
    	
		return userToString(obj);
    }
    
    /**
     * EX: "http://localhost:5000/username?name={Name}"
     * @param name
     * @return retString
     */
    @GetMapping("/username")
    @ResponseStatus(HttpStatus.OK)
    public String nameUser(@RequestParam(value = "name")String name) {
    	
    	name = StringEscapeUtils.escapeHtml4(name);
    	
    	String output = "";
    	JSONObject obj = null;
    	JSONArray myArr = null;
    	String retString = "Usernames with a name of " + name + ":<br/>-----------------------------------------<br/>";
    	try {
    		URL url = new URL("http://localhost:8591/users?filter=username::" + name);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		
    		conn.setRequestMethod("GET");
    		conn.setRequestProperty("Accept", "application/json");
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());
    		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		
    		output = br.readLine();
    		obj = new JSONObject(output);
    		br.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    	}
    	
    	if(obj == null) {
    		return "Error: Object not found";
    	}
    	myArr = obj.getJSONObject("_embedded").getJSONArray("users");
		
    	for(int i = 0; i < myArr.length(); i++) {
    		JSONObject users = myArr.getJSONObject(i);
    		retString += userToStringPartial(users) + "<br/>";

    	}
    	
    	return retString;
    }
    
    /**
     * EX: "http://localhost:5000/adduser?username={username}&password={password}"
     * @param username
     * @param password
     * @return output
     */
    @GetMapping("/adduser")
    @ResponseStatus(HttpStatus.CREATED)
    public String addUser(@RequestParam(value = "username")String username, 
    		@RequestParam(value = "password")String password) {
    	String output = "";
    	JSONObject obj = null;
    	try {
    		URL url = new URL("http://localhost:8591/users");
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    				
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
    	    conn.setRequestProperty("Accept", "application/json");
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());
    	    conn.setDoOutput(true);
    	    
    	    JSONObject newApp = new JSONObject();
    	    newApp.put("username", username);
    	    newApp.put("password", password);
    	    
    	    OutputStream os = conn.getOutputStream();
    	    os.write(newApp.toString().getBytes("UTF-8"));
    	    os.close();
    	    
    	    BufferedReader br = new BufferedReader(new InputStreamReader(
    	                    (conn.getInputStream())));

    	    output = br.readLine();
    	            
    	    obj = new JSONObject(output); 
    	    br.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    	}
    	
    	return userToString(obj);
    }
    
    /**
     * EX: "http://localhost:5000/deleteuser?oid={ObjectID}&adminKey={adminkey}"
     * @param oid
     * @return retString
     */
    @GetMapping("/deleteuser")
    @ResponseStatus(HttpStatus.OK)
    public String deleteUser(@RequestParam(value = "oid")String oid, 
    		@RequestParam(value = "adminKey")String adminKey) {
    	if(adminKey.compareTo(this.adminKey) != 0) {
    		return "Error: invalid admin key!";
    	}
    	try {
    		URL url = new URL("http://localhost:8591/users/" + oid);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		
    	    conn.setRequestProperty("Accept", "application/json");
    	    conn.setDoOutput(true);
    	    conn.setRequestProperty("Authorization", "Bearer " + getJwt());		
    		conn.setRequestMethod("DELETE");
    	    //conn.connect();
    		if(conn.getResponseCode() != 204) {
    			return "Error occurred in deleting user";
    		}
    		System.out.println(conn.getResponseCode());
    	    
    	} catch(Exception e) {
    		System.out.println(e.toString());
    		return "Error occurred in deleting user";
    	}
    	  	
		return "User successfully deleted.";
    }
    
	/**
     * EX: "http://localhost:5000/pinguser"
     * @return retString
     */
    @GetMapping("/pinguser")
    @ResponseStatus(HttpStatus.OK)
    public String pingUser() {
    	String retString = "";
    	try {
    		URL url = new URL("http://localhost:8591/users/utils/ping");
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		
    	    conn.setRequestProperty("Accept", "application/json");	
    		conn.setRequestMethod("GET");
    		if(conn.getResponseCode() == 200) {
    			retString = "User service is currently running";
    		}
    	} catch(Exception e) {
    		System.out.println(e.toString());
    		retString = "Warning: Cannot connect to User Service";
    	}
    	return retString;
    }
    
    /**
     * Converts JSON client object to a string detailing each field
     * @param theUser
     * @return retString
     */
    private String userToString(JSONObject theUser) {
    	String retString = "";
    	retString += "Username: " + theUser.getString("username");
    	retString += "<br/>Password: " + theUser.getString("password");
    	retString += "<br/>Object ID: " + theUser.getString("id");
    	retString += "<br/>Created At: " + theUser.getString("createdAt");
    	retString += "<br/>Updated At: " + theUser.getString("updatedAt") + "<br/>";
    	return retString;
    }
    
    /**
     * Converts JSON client object to a string detailing the username, and created at and updated at times
     * @param theUser
     * @return retString
     */
    private String userToStringPartial(JSONObject theUser) {
    	String retString = "";
    	retString += "Username: " + theUser.getString("username");
    	retString += "<br/>Created At: " + theUser.getString("createdAt");
    	retString += "<br/>Updated At: " + theUser.getString("updatedAt") + "<br/>";
    	return retString;
    }
    
    /**
     * Loads application.properties to get admin key value
     * 
     * @return prop = properties config
     */
    private Properties getProperties() {
 	   InputStream inStream;
 	   
 	   try {
 		   Properties prop = new Properties();
 		   inStream = getClass().getClassLoader().getResourceAsStream(FILENAME);
 		   
 		   if(inStream != null) {
 			   prop.load(inStream);
 		   } else {
 			   throw new FileNotFoundException("property file" + FILENAME + " not found");
 		   }
 		   
 		   this.adminKey = prop.getProperty("adminKey");
 		   
 		   inStream.close();
 	   } catch(Exception e) {
 		   System.out.println("Exception: " + e);
 	   }
 	   
 	   return prop;
    }
    
    /**
     * Gets the JWT set in application.properties
     * 
     * @return jwt
     */
    private String getJwt() {
 	   String jwt = "";
 	   InputStream inStream;
 	   
 	   try {
 		   Properties prop = new Properties();
 		   inStream = getClass().getClassLoader().getResourceAsStream(FILENAME);
 		   
 		   if(inStream != null) {
 			   prop.load(inStream);
 		   } else {
 			   throw new FileNotFoundException("property file" + FILENAME + " not found");
 		   }
 		   jwt = prop.getProperty("jwt");
 		   
 		   inStream.close();
 	   } catch(Exception e) {
 		   System.out.println("Exception: " + e);
 	   }
 	   return jwt;
    }
}
