package TestApp.Application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * Spring Boot application starter class.
 */
@SpringBootApplication
public class Application {
	
	/**
	 * 
	 * @param args
	 */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
