FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /Application/target/springboot-rest-api-sample-1.0.0-SNAPSHOT.jar app.jar
ADD /Authentication/target/Authentication-1.0-SNAPSHOT.jar app.jar
ADD /Vehicle/target/Vehicle-1.0-SNAPSHOT.jar app.jar
ADD /Client/target/Client-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
