package com.example.authenticate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.restexpress.Request;

/**
 *
 * 
 */
public class AuthenticateJwt {

    /**
     *
     * @param request
     * @param baseUrl
     * @return
     */
    public boolean authenticateJwt(Request request, String baseUrl) {
        String jwt, output, valid = "";

        try {
            jwt = (request.getHeader("Authorization").split(" "))[1];
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            System.out.println(e);
            return false;
        }

        try {
            URL url = new URL("http://" + baseUrl + ":8587" + "/jwts/" + jwt);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                return false;
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            //LOG.info("Output from Server:");
            while ((output = br.readLine()) != null) {
                valid = output;
            }

            conn.disconnect();
        } catch (MalformedURLException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
        return Boolean.parseBoolean(valid);
    }
}
