package com.example.client.objectid;

import org.restexpress.plugin.hyperexpress.Linkable;
import com.strategicgains.repoexpress.mongodb.AbstractMongodbEntity;
import org.mongodb.morphia.annotations.Entity;

/**
 * This is a User entity identified by a MongoDB ObjectID (instead of a
 * UUID). It also contains createdAt and updatedAt properties that are
 * automatically maintained by the persistence layer (UserRepository).
 */
@Entity("User")
public class User 
        extends AbstractMongodbEntity
        implements Linkable {
    
        private String username;
        private String password;
        
    /**
     *
     */
    public User() {
    }

    /**
     *
     * @param username
     * @param password

     */
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        return getUsername() + " " + getPassword();
    }
    /**
     * @return 
     * @return the username
     */
    public String getUsername() {
        return username;
    }
    /**
     * @return 
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param pass to set the password
     */
    public void setPassword(String pass){
        this.password = pass;
    }
    /**
     * 
     * @param username get set the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

}
