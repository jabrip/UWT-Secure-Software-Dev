package com.example.client.objectid;

import java.util.List;

import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;

import com.strategicgains.repoexpress.domain.Identifier;
import com.strategicgains.syntaxe.ValidationEngine;

/**
 * This is the 'service' or 'business logic' layer, where business logic,
 * syntactic and semantic domain validation occurs, along with calls to the
 * persistence layer.
 */
public class UserService {

    private final UserRepository users;

    /**
     *
     * @param UserRepository
     */
    public UserService(UserRepository userRepository) {
        super();
        this.users = userRepository;
    }

    /**
     *
     * @param entity
     * @return
     */
    public User create(User entity) {
        ValidationEngine.validateAndThrow(entity);
        return users.create(entity);
    }

    /**
     *
     * @param id
     * @return
     */
    public User read(Identifier id) {
        return users.read(id);
    }

    /**
     *
     * @param entity
     */
    public void update(User entity) {
        ValidationEngine.validateAndThrow(entity);
        users.update(entity);
    }

    /**
     *
     * @param id
     */
    public void delete(Identifier id) {
        users.delete(id);
    }

    /**
     *
     * @param filter
     * @param range
     * @param order
     * @return
     */
    public List<User> readAll(QueryFilter filter, QueryRange range, QueryOrder order) {
        return users.readAll(filter, range, order);
    }

    /**
     *
     * @param filter
     * @return
     */
    public long count(QueryFilter filter) {
        return users.count(filter);
    }
}
