package com.example.client.objectid;

import com.mongodb.MongoClient;
import com.strategicgains.repoexpress.mongodb.MongodbEntityRepository;

/**
 *
 * @author Tung Dang
 */
public class UserRepository
        extends MongodbEntityRepository<User> {

    /**
     *
     * @param mongo
     * @param dbName
     */
    @SuppressWarnings("unchecked")
    public UserRepository(MongoClient mongo, String dbName) {
        super(mongo, dbName, User.class);
    }
}
