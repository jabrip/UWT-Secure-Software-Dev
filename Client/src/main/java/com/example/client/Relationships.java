package com.example.client;

import java.util.Map;

import org.restexpress.RestExpress;
import com.example.client.objectid.User;

import com.strategicgains.hyperexpress.HyperExpress;
import com.strategicgains.hyperexpress.RelTypes;

/**
 *
 * @author Jabriel Phan
 */
public abstract class Relationships {

    /**
     *
     * @param server
     */
    public static void define(RestExpress server) {
        Map<String, String> routes = server.getRouteUrlsByName();

        HyperExpress.relationships()
                .forCollectionOf(User.class)
                .rel(RelTypes.SELF, routes.get(Constants.Routes.USER_COLLECTION))
                .withQuery("limit={limit}")
                .withQuery("offset={offset}")
                .rel(RelTypes.NEXT, routes.get(Constants.Routes.USER_COLLECTION) + "?offset={nextOffset}")
                .withQuery("limit={limit}")
                .optional()
                .rel(RelTypes.PREV, routes.get(Constants.Routes.USER_COLLECTION) + "?offset={prevOffset}")
                .withQuery("limit={limit}")
                .optional()
                .forClass(User.class)
                .rel(RelTypes.SELF, routes.get(Constants.Routes.SINGLE_USER))
                .rel(RelTypes.UP, routes.get(Constants.Routes.USER_COLLECTION));
    }
}
