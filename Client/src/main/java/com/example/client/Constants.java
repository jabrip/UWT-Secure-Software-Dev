package com.example.client;

/**
 *
 * @author Thomas N.
 */
public class Constants {

    /**
     * These define the URL parameters used in the route definition strings
     * (e.g. '{userId}').
     */
    public class Url {
       
        public static final String USER_ID = "oid";
    }

    /**
     * These define the route names used in naming each route definitions. These
     * names are used to retrieve URL patterns within the controllers by name to
     * create links in responses.
     */
    public class Routes {

        public static final String SINGLE_USER = "client.single.route";
        public static final String USER_COLLECTION = "client.collection.route";
    }
}